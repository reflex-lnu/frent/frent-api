import { Client } from "pg";

export const db = new Client({
  host: process.env.PG_HOST || "localhost",
  port: Number(process.env.PG_PORT) || 5432,
  database: process.env.PG_DATABASE || "frent",
  user: process.env.PG_USERNAME || "postgres",
  password: process.env.PG_PASSWORD || "admin",
});
