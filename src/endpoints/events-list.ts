import {Type} from "@sinclair/typebox";
import {db} from "../db";
import {TypeProvidedRouteShorthandOptionsWithHandler} from "../types";
import {auth} from "../middleware";

const schema = {
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          id: Type.Number(),
          title: Type.String(),
          description: Type.String(),
          startsAt: Type.String(),
          endsAt: Type.String(),
          isRecurring: Type.Boolean()
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

export const eventsListOpts: TypeProvidedRouteShorthandOptionsWithHandler<typeof schema> = {
  schema,
  preHandler: auth as any,
  handler: async (req, res) => {

    const {rows} = await db.query(
      `
          SELECT *
          FROM events.event as e
      `
    );

    res.status(200).send({
      items: rows.map((r) => ({
        id: r.id,
        title: r.title,
        description: r.description,
        startsAt: r.starts_at,
        endsAt: r.ends_at,
        isRecurring: r.is_recurring
      })),
    });
  },
};
