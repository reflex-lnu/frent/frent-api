import {Type} from "@sinclair/typebox";
import {db} from "../../db";
import {TypeProvidedRouteShorthandOptionsWithHandler} from "../../types";
import {auth} from "../../middleware";

const schema = {
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          clientId: Type.Number(),
          clientFullName: Type.String(),
          partyStartsAt: Type.String(),
          timesRented: Type.Number(),
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

const query = `
    SELECT EXTRACT(MONTH FROM p.starts_at) as month,
           COUNT(*)                        as parties
    FROM events.parties as p
    GROUP BY EXTRACT(MONTH FROM p.starts_at);

`

export const rq6Opts: TypeProvidedRouteShorthandOptionsWithHandler<typeof schema> = {
  schema,
  handler: async (req, res) => {

    const {rows} = await db.query(
      query
    );

    res.status(200).send({
      items: rows
    });
  },
};
