import {Type} from "@sinclair/typebox";
import {db} from "../../db";
import {TypeProvidedRouteShorthandOptionsWithHandler} from "../../types";
import {auth} from "../../middleware";

const schema = {
  querystring: Type.Object({
    clientId: Type.Number(),
    friendId: Type.Number(),
    from: Type.String(),
    to: Type.String()
  }),
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          eventId: Type.Number(),
          eventTitle: Type.String(),
          partyStartsAt: Type.Number()
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

const query = `
    SELECT p.event_id as eventId,
           e.title    as eventTitle,
           p.startsAt as partyStartsAt
    FROM events.parties as p
             INNER JOIN events.event as e
                        ON e.id = p.event_id
    WHERE p.creator_client_id = $3
      AND p.starts_at BETWEEN $3 AND $4
      AND p.id IN (SELECT pf.party_id FROM events.party_friend as pf WHERE pf.friend_id = $2);

`

export const rq10Opts: TypeProvidedRouteShorthandOptionsWithHandler<typeof schema> = {
  schema,
  handler: async (req, res) => {
    const {clientId, friendId, from, to} = req.query;

    const {rows} = await db.query(
      query,
      [clientId, friendId, from, to]
    );

    res.status(200).send({
      items: rows
    });
  },
};
