import {Type} from "@sinclair/typebox";
import {db} from "../../db";
import {TypeProvidedRouteShorthandOptionsWithHandler} from "../../types";
import {auth} from "../../middleware";

const schema = {
  querystring: Type.Object({
    from: Type.String(),
    to: Type.String(),
    n: Type.Number()
  }),
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          friendId: Type.Number(),
          friendFullName: Type.String(),
          partyStartsAt: Type.String(),
          timesRented: Type.Number()
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

const query = `
    SELECT pf.friend_id as friendId,
           f.full_name  as friendFullName,
           p.starts_at  as partyStartsAt,
           COUNT(*)     as timesRented
    FROM events.party_friend AS pf
             INNER JOIN events.party AS p
                        ON pf.party_id = p.id
    WHERE p.starts_at BETWEEN $1 AND $2
    GROUP BY pf.friend_id
    HAVING COUNT(*) >= $3;
`

export const rq5Opts: TypeProvidedRouteShorthandOptionsWithHandler<typeof schema> = {
  schema,
  handler: async (req, res) => {
    const {from, to, n} = req.query;

    const {rows} = await db.query(
      query,
      [from, to, n]
    );

    res.status(200).send({
      items: rows
    });
  },
};
