import {Type} from "@sinclair/typebox";
import {db} from "../../db";
import {TypeProvidedRouteShorthandOptionsWithHandler} from "../../types";
import {auth} from "../../middleware";

const schema = {
  querystring: Type.Object({
    a: Type.Number(),
    b: Type.Number()
  }),
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          date: Type.String(),
          friendsOnVacation: Type.Number()
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

const query = `
    SELECT v.date,
           COUNT(v.friend_id) as friendsOnVacation
    FROM users.vacation as v
    GROUP BY v.date
    HAVING COUNT(v.friend_id) BETWEEN $1 AND $2;

`

export const rq11Opts: TypeProvidedRouteShorthandOptionsWithHandler<typeof schema> = {
  schema,
  handler: async (req, res) => {
    const {a, b} = req.query;

    const {rows} = await db.query(
      query,
      [a, b]
    );

    res.status(200).send({
      items: rows
    });
  },
};
