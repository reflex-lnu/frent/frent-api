import {Type} from "@sinclair/typebox";
import {db} from "../../db";
import {TypeProvidedRouteShorthandOptionsWithHandler} from "../../types";
import {auth} from "../../middleware";

const schema = {
  querystring: Type.Object({
      friendId: Type.Number(),
      from: Type.String(),
      to: Type.String(),
      n: Type.Number()
    }),
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          eventId: Type.Number(),
          eventTitle: Type.String(),
          timesRented: Type.Number(),
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

const query = `
    SELECT sub.event_id        as eventId,
           e.title             as eventTitle,
           COUNT(sub.party_id) AS timesRented
    FROM (SELECT p.event_id AS event_id, p.id AS party_id
          FROM events.party_friend AS pf
                   INNER JOIN events.party AS p ON pf.party_id = p.id
                   LEFT JOIN events.event AS e ON e.id = p.event_id
          WHERE pf.friend_id = $1
            AND p.starts_at BETWEEN $2 AND $3
          GROUP BY p.event_id, p.id
          HAVING COUNT(*) > $4) AS sub
             INNER JOIN events.event as e
                        ON e.id = sub.event_id
    GROUP BY sub.event_id, e.title;

`

export const rq7Opts: TypeProvidedRouteShorthandOptionsWithHandler<typeof schema> = {
  schema,
  handler: async (req, res) => {
    const {friendId, from, to, n} = req.query;

    const {rows} = await db.query(
      query,
      [friendId, from, to, n]
    );

    res.status(200).send({
      items: rows
    });
  },
};
