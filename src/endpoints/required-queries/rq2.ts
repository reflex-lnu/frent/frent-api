import {Type} from "@sinclair/typebox";
import {db} from "../../db";
import {TypeProvidedRouteShorthandOptionsWithHandler} from "../../types";
import {auth} from "../../middleware";

const schema = {
  querystring: Type.Object({
    friendId: Type.Number(),
    clientId: Type.Number(),
    from: Type.String(),
    to: Type.String(),
  }),
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          clientId: Type.Number(),
          clientFullName: Type.String(),
          partyStartsAt: Type.String(),
          timesRented: Type.Number(),
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

const query = `
    SELECT p.creator_client_id as clientId,
           u.full_name         as clientFullName,
           p.starts_at         as partyStartsAt,
           COUNT(*)            as timesRented
    FROM events.party AS p
             INNER JOIN events.party_friend AS pf
                        ON pf.party_id = p.id
    WHERE pf.friend_id = $1
      AND p.starts_at BETWEEN $2 AND $3
    GROUP BY p.creator_client_id, u.full_name, p.starts_at
    HAVING COUNT(*) >= $4;
`

export const rq2Opts: TypeProvidedRouteShorthandOptionsWithHandler<typeof schema> = {
  schema,
  handler: async (req, res) => {
    const {friendId, clientId, from, to} = req.query;

    const {rows} = await db.query(
      query,
      [friendId, clientId, from, to]
    );

    res.status(200).send({
      items: rows
    });
  },
};
