import {Type} from "@sinclair/typebox";
import {db} from "../../db";
import {TypeProvidedRouteShorthandOptionsWithHandler} from "../../types";
import {auth} from "../../middleware";

const schema = {
  querystring: Type.Object({
    from: Type.String(),
    to: Type.String(),
    n: Type.Number()
  }),
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          clientId: Type.Number(),
          clientFullName: Type.String(),
          partyStartsAt: Type.String(),
          timesRented: Type.Number(),
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

const query = `
    SELECT p.creator_client_id as clientId,
           u.full_name         as clientFullName,
           p.starts_at         as partyStartsAt,
           COUNT(pf.friend_id)
    FROM events.party AS p
             INNER JOIN users.user as u
                        ON u.id = p.creator_client_id
             INNER JOIN events.party_friend AS pf
                        ON p.id = pf.party_id
    WHERE p.starts_at BETWEEN $1 AND $2
    GROUP BY p.creator_client_id, u.full_name, p.starts_at
    HAVING COUNT(DISTINCT pf.friend_id) >= $3;
`

export const rq4Opts: TypeProvidedRouteShorthandOptionsWithHandler<typeof schema> = {
  schema,
  handler: async (req, res) => {
    const {from, to, n} = req.query;

    const {rows} = await db.query(
      query,
      [from, to, n]
    );

    res.status(200).send({
      items: rows
    });
  },
};
