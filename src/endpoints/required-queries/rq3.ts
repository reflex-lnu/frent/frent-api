import {Type} from "@sinclair/typebox";
import {db} from "../../db";
import {TypeProvidedRouteShorthandOptionsWithHandler} from "../../types";
import {auth} from "../../middleware";

const schema = {
  querystring: Type.Object({
    friendId: Type.Number(),
    from: Type.String(),
    to: Type.String(),
    n: Type.Number()
  }),
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          eventId: Type.Number(),
          eventTitle: Type.String(),
          timesRented: Type.Number()
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

const query = `
    SELECT p.event_id AS eventId,
           e.title    as eventTitle,
           COUNT(*)   as timesRented
    FROM events.party AS p
             INNER JOIN events.party_friend AS pf
                        ON p.id = pf.party_id
             INNER JOIN events.event AS e
                        ON e.id = p.event_id
    WHERE pf.id = $1
      AND p.starts_at BETWEEN $2 AND $3
    GROUP BY p.event_id, e.title
    HAVING COUNT(*) >= $4;
`

export const rq3Opts: TypeProvidedRouteShorthandOptionsWithHandler<typeof schema> = {
  schema,
  handler: async (req, res) => {
    const {friendId, from, to, n} = req.query;

    const {rows} = await db.query(
      query,
      [friendId, from, to, n]
    );

    res.status(200).send({
      items: rows
    });
  },
};
