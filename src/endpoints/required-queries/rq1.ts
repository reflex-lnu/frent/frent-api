import { Type } from "@sinclair/typebox";
import { db } from "../../db";
import { TypeProvidedRouteShorthandOptionsWithHandler } from "../../types";

const schema = {
  querystring: Type.Object({
    clientId: Type.Number(),
    n: Type.Number(),
    from: Type.String(),
    to: Type.String(),
  }),
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          friendId: Type.Number(),
          friendFullName: Type.String(),
          partyStartsAt: Type.String(),
          timesRented: Type.Number(),
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

export const rq1Opts: TypeProvidedRouteShorthandOptionsWithHandler<
  typeof schema
> = {
  schema,
  handler: async (req, res) => {

    const { clientId, n, from, to } = req.query;

    const { rows } = await db.query(
      `
      SELECT f.id as friendId, u.full_name as friendFullName, p.starts_at as partyStartsAt, COUNT(*) as timesRented
      FROM users.friend AS f
      INNER JOIN users.user AS u
      ON u.id = f.user_id
      INNER JOIN events.party_friend AS pf
      ON f.id = pf.friend_id
      INNER JOIN events.party AS p
      ON p.id = pf.party_id
      WHERE p.creator_client_id = $1
      AND p.starts_at BETWEEN $2 AND $3
      GROUP BY f.id, u.full_name, p.starts_at
      HAVING COUNT(*) >= $4;
      `,
        [clientId, from, to, n]
    );

    res.status(200).send({
      items: rows.map((r) => ({
        friendId: r.friendId,
        friendFullName: r.friendFullName,
        partyStartsAt: r.partyStartsAt,
        timesRented: r.timesRented,
      })),
    });
  },
};
