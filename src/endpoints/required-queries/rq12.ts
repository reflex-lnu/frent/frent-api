import {Type} from "@sinclair/typebox";
import {db} from "../../db";
import {TypeProvidedRouteShorthandOptionsWithHandler} from "../../types";
import {auth} from "../../middleware";

const schema = {
  querystring: Type.Object({
    friendId: Type.Number()
  }),
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          month: Type.Number(),
          complaints: Type.Number(),
          averageComplaintSigns: Type.Number()
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

const query = `
    SELECT EXTRACT(MONTH FROM sub.created_at) as month,
           COUNT(*)                           as complaints,
           AVG(sub.n_signs)                   as averageComplaintSigns
    FROM (SELECT c.created_at,
                 COUNT(*) as n_signs
          FROM feedbacks.complaint as c
                   INNER JOIN feedbacks.complaint_signer as cs
                              ON cs.complaint_party_friend_id = c.party_friend_id
                   INNER JOIN events.party_friend as pf
                              ON pf.id = c.party_friend_id
          WHERE pf.friend_id = $1) as sub
    GROUP BY EXTRACT(MONTH FROM sub.created_at);

`

export const rq12Opts: TypeProvidedRouteShorthandOptionsWithHandler<typeof schema> = {
  schema,
  handler: async (req, res) => {
    const {friendId} = req.query;

    const {rows} = await db.query(
      query,
      [friendId]
    );

    res.status(200).send({
      items: rows
    });
  },
};
