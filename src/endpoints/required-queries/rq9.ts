import {Type} from "@sinclair/typebox";
import {db} from "../../db";
import {TypeProvidedRouteShorthandOptionsWithHandler} from "../../types";
import {auth} from "../../middleware";

const schema = {
  querystring: Type.Object({
    from: Type.String(),
    to: Type.String(),
    n: Type.Number(),
  }),
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          friendId: Type.Number(),
          friendFullName: Type.String(),
          complaints: Type.Number(),
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

const query = `
    SELECT sub.friend_id as friendId,
           u.full_name   as friendFullName,
           sub.n         as complaints
    FROM (SELECT c.party_friend_id, COUNT(*) as n
          FROM feedbacks.complaint as c
                   INNER JOIN feedbacks.complaint_sign as cs
                              ON cs.complaint_party_friend_id = c.party_friend_id
          WHERE c.created_at BETWEEN $1 AND $2
          HAVING COUNT(*) >= $3) as sub
             INNER JOIN users.friend as f
                        ON f.id = sub.friend_id
             INNER JOIN users.user as u
                        ON u.id = f.user_id
    ORDER BY complaints DESC;

`

export const rq9Opts: TypeProvidedRouteShorthandOptionsWithHandler<typeof schema> = {
  schema,
  handler: async (req, res) => {
    const {from, to, n} = req.query;

    const {rows} = await db.query(
      query,
      [from, to, n]
    );

    res.status(200).send({
      items: rows
    });
  },
};
