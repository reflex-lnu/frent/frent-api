import {Type} from "@sinclair/typebox";
import {db} from "../../db";
import {TypeProvidedRouteShorthandOptionsWithHandler} from "../../types";
import {auth} from "../../middleware";

const schema = {
  querystring: Type.Object({
    clientId: Type.Number(),
    from: Type.String(),
    to: Type.String()
  }),
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          giftId: Type.Number(),
          giftDescription: Type.String(),
          vacations: Type.Number(),
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

const query = `
    SELECT sub.gift_id              as giftId,
           g.description,
           AVG(sub.vacations_count) as vacations
    FROM (SELECT gv.gift_id, COUNT(v.*) AS vacations_count
          FROM gifts.giving AS gv
                   INNER JOIN gifts.gift AS g ON gv.gift_id = g.id
                   LEFT JOIN users.vacation AS v ON recipient_friend_id = v.friend_id
          WHERE g.creator_client_id = $1
            AND gv.given_at BETWEEN $2 AND $3
          GROUP BY gv.gift_id, gv.recipient_friend_id) AS sub
             INNER JOIN gifts.gift as g
                        ON g.id = sub.gift_id
    GROUP BY sub.gift_id
    ORDER BY average_vacations_count DESC;

`

export const rq8Opts: TypeProvidedRouteShorthandOptionsWithHandler<typeof schema> = {
  schema,
  handler: async (req, res) => {
    const { clientId, from, to } = req.query;

    const {rows} = await db.query(
      query,
      [clientId, from, to]
    );

    res.status(200).send({
      items: rows
    });
  },
};
