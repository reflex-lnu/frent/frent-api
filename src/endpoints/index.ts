import { FastifyInstance } from "fastify";
import { TypeProvidedFastifyInstance } from "../types";
import { loginOpts } from "./login";
import { registerOpts } from "./register";
import {eventsListOpts} from "./events-list";
import {partiesListOpts} from "./parties-list";
import {partyFriendsListOpts} from "./party-friends-list";
import {partyComplaintsListOpts} from "./party-complaints-list";
import { rq1Opts } from "./required-queries/rq1";
import { rq2Opts } from "./required-queries/rq2";
import { rq3Opts } from "./required-queries/rq3";
import { rq4Opts } from "./required-queries/rq4";
import { rq5Opts } from "./required-queries/rq5";
import { rq6Opts } from "./required-queries/rq6";
import { rq7Opts } from "./required-queries/rq7";
import { rq8Opts } from "./required-queries/rq8";
import { rq9Opts } from "./required-queries/rq9";
import { rq10Opts } from "./required-queries/rq10";
import { rq11Opts } from "./required-queries/rq11";
import { rq12Opts } from "./required-queries/rq12";

export const mountEndpoints = (
  fastify: TypeProvidedFastifyInstance,
  _options: any,
  done: any
) => {
  fastify.post("/login", loginOpts);
  fastify.post("/register", registerOpts);

  fastify.get("/events", eventsListOpts);
  fastify.get("/parties", partiesListOpts);
  fastify.get("/parties/:partyId/friends", partyFriendsListOpts);
  fastify.get("/parties/:partyId/complaints", partyComplaintsListOpts);

  fastify.get("/required-query/1", rq1Opts);
  fastify.get("/required-query/2", rq2Opts);
  fastify.get("/required-query/3", rq3Opts);
  fastify.get("/required-query/4", rq4Opts);
  fastify.get("/required-query/5", rq5Opts);
  fastify.get("/required-query/6", rq6Opts);
  fastify.get("/required-query/7", rq7Opts);
  fastify.get("/required-query/8", rq8Opts);
  fastify.get("/required-query/9", rq9Opts);
  fastify.get("/required-query/10", rq10Opts);
  fastify.get("/required-query/11", rq11Opts);
  fastify.get("/required-query/12", rq12Opts);
  done();
};
