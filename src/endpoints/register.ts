import { sign } from "jsonwebtoken";
import { sha512 } from "js-sha512";
import { Type } from "@sinclair/typebox";
import { db } from "../db";
import { TypeProvidedRouteShorthandOptionsWithHandler } from "../types";
import { nanoid } from "nanoid";

const schema = {
  body: Type.Object({
    email: Type.String({ minLength: 1, maxLength: 256 }),
    password: Type.String({ minLength: 1, maxLength: 512 }),
    role: Type.Union([Type.Literal('client'), Type.Literal('friend')]),
    fullName: Type.String(),
    birthDate: Type.String(),
    sex: Type.String(),
    phoneNumber: Type.Number(),
    address: Type.String(),
  }),
  response: {
    200: Type.Object({
      token: Type.String(),
    }),
    409: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

export const registerOpts: TypeProvidedRouteShorthandOptionsWithHandler<
  typeof schema
> = {
  schema,
  handler: async (req, res) => {
    const { email, password, role, fullName, birthDate, sex, phoneNumber, address } =
      req.body;

    const existingUser = (
      await db.query(
        "SELECT id, pass_hash, pass_salt FROM users.user WHERE email = $1::text LIMIT 1",
        [email]
      )
    ).rows[0];

    if (existingUser) {
      res.status(409).send({ message: "User with such email already exists" });
    }

    const passSalt = nanoid(17);
    const passHash = sha512(password + passSalt);

    const {
      rows: [id],
    } = await db.query(`INSERT INTO users.user (email, pass_hash, pass_salt, full_name, birth_date, sex, address, phone_number)VALUES
    ('${email}',
     '${passHash}', 
     '${passSalt}',
     '${fullName}',
     '${new Date(birthDate).toISOString().substring(0, 10)}',
     '${sex}',
     '${address}',
     ${phoneNumber} )
    `);

    const {
      rows: [roleId],
    } = await db.query(`INSERT INTO users.${role} (user_id)VALUES ($1)`,
      [id]);

    const token = sign({ id, [`${role}Id`]: roleId }, process.env.JWT_SECRET || "default");

    res.status(200).send({ token });
  },
};
