import { sign } from "jsonwebtoken";
import { sha512 } from "js-sha512";
import { Type } from "@sinclair/typebox";
import { db } from "../db";
import { TypeProvidedRouteShorthandOptionsWithHandler } from "../types";
import { auth } from "../middleware";

const schema = {
  body: Type.Object({
    email: Type.String({ minLength: 1, maxLength: 256 }),
    password: Type.String({ minLength: 1, maxLength: 512 }),
  }),
  response: {
    200: Type.Object({
      token: Type.String(),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

export const loginOpts: TypeProvidedRouteShorthandOptionsWithHandler<
  typeof schema
> = {
  schema,
  handler: async (req, res) => {
    const { email, password } = req.body;

    const user = (
      await db.query(
        "SELECT id, pass_hash, pass_salt FROM users.user WHERE email = $1::text LIMIT 1",
        [email]
      )
    ).rows[0];

    if (!user) {
      res.status(404).send({ message: "User not found" });
    }

    const passwordHash = sha512(password + user.pass_salt);
    if (user.pass_hash !== passwordHash) {
      res.status(404).send({ message: "Wrong password" });
    }

    const role = ((
      await db.query(
        `SELECT id, 'client' as role FROM users.client WHERE user_id = $1`,
        [user.id]
      )
    ).rows[0]) || ((
      await db.query(
        `SELECT id, 'friend' as role FROM users.friend WHERE user_id = $1`,
        [user.id]
      )
    ).rows[0]);

    if(!role) {
      res.status(404).send({ message: "Role entry not found" });
    }

    const token = sign({ id: user.id, [`${role.role}Id`]: role.id }, process.env.JWT_SECRET || "default");

    res.status(200).send({ token });
  },
};
