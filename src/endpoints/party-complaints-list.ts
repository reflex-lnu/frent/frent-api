import { decode } from "jsonwebtoken";
import { Type } from "@sinclair/typebox";
import { db } from "../db";
import { TypeProvidedRouteShorthandOptionsWithHandler } from "../types";
import { auth } from "../middleware";

const schema = {
  params: {
    partyId: Type.Number()
  },
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          partyFriendId: Type.Number(),
          message: Type.String(),
          createdAt: Type.String(),
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

export const partyComplaintsListOpts: TypeProvidedRouteShorthandOptionsWithHandler<
  typeof schema
> = {
  schema,
  preHandler: auth as any,
  handler: async (req, res) => {
    const token = req.headers.authorization?.split(" ")[1];
    const { id, clientId } = decode(token);

    if(!clientId) {
      res.status(403).send({ message: "Allowed only for clients" });
    }

    // @ts-ignore
    const { partyId } = req.params;

    const party = (
      await db.query(
      `SELECT id FROM events.party WHERE id = $1 AND creator_client_id = $2`,
      [partyId, clientId]
      )
    ).rows[0];

    if(!party) {
      res.status(404).send({ message: "Party not found" });
    }

    const { rows } = await db.query(
      `
        SELECT c.*
        FROM events.party as p
        INNER JOIN events.party_friend as pf
        ON pf.party_id = p.id
        INNER JOIN feedbacks.complaint as c
        ON c.party_friend_id = pf.id
        WHERE p.id = $1
      `,
        [partyId]
    );

    res.status(200).send({
      items: rows.map((r) => ({
        partyFriendId: r.party_friend_id,
        message: r.message,
        createdAt: r.created_at
      })),
    });
  },
};
