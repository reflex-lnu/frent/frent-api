import { decode } from "jsonwebtoken";
import { Type } from "@sinclair/typebox";
import { db } from "../db";
import { TypeProvidedRouteShorthandOptionsWithHandler } from "../types";
import { auth } from "../middleware";

const schema = {
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          id: Type.Number(),
          creatorClientId: Type.Number(),
          eventId: Type.Number(),
          title: Type.String(),
          description: Type.String(),
          startsAt: Type.String(),
          endsAt: Type.String(),
          address: Type.String(),
          createdAt: Type.String()
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

export const partiesListOpts: TypeProvidedRouteShorthandOptionsWithHandler<
  typeof schema
> = {
  schema,
  preHandler: auth as any,
  handler: async (req, res) => {
    const token = req.headers.authorization?.split(" ")[1];
    const { id } = decode(token);

    const { rows } = await db.query(
      `
        SELECT *
        FROM events.party as p
        WHERE p.creator_client_id = $1
      `,
        [id]
    );

    res.status(200).send({
      items: rows.map((r) => ({
        id: r.id,
        creatorClientId: r.creator_client_id,
        eventId: r.event_id,
        title: r.title,
        description: r.description,
        startsAt: r.starts_at,
        endsAt: r.ends_at,
        address: r.address,
        createdAt: r.created_at
      })),
    });
  },
};
