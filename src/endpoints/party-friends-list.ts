import { decode } from "jsonwebtoken";
import { Type } from "@sinclair/typebox";
import { db } from "../db";
import { TypeProvidedRouteShorthandOptionsWithHandler } from "../types";
import { auth } from "../middleware";

const schema = {
  params: {
    partyId: Type.Number()
  },
  response: {
    200: Type.Object({
      items: Type.Array(
        Type.Object({
          id: Type.Number(),
          fullName: Type.String(),
          birthDate: Type.String(),
          sex: Type.String(),
          email: Type.String(),
          phoneNumber: Type.Number(),
          address: Type.String()
        })
      ),
    }),
    404: Type.Object({
      message: Type.String(),
    }),
  },
} as const;

export const partyFriendsListOpts: TypeProvidedRouteShorthandOptionsWithHandler<
  typeof schema
> = {
  schema,
  preHandler: auth as any,
  handler: async (req, res) => {
    const token = req.headers.authorization?.split(" ")[1];
    const { id } = decode(token);
    // @ts-ignore
    const { partyId } = req.params;

    const { rows } = await db.query(
      `
        SELECT u.*
        FROM events.parties as p
        INNER JOIN events.party_friend as pf
        ON p.id = pf.party_id
        INNER JOIN users.friend as f
        ON f.id = pf.friend_id
        INNER JOIN users.user as u
        ON u.id = f.user_id
        WHERE p.id = $1
      `,
        [partyId]
    );

    res.status(200).send({
      items: rows.map((r) => ({
        id: r.id,
        fullName: r.full_name,
        birthDate: r.birth_date,
        sex: r.sex,
        email: r.email,
        phoneNumber: r.phone_number,
        address: r.address
      })),
    });
  },
};
