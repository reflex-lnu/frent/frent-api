import fastify from "fastify";
import { TypeBoxTypeProvider } from "@fastify/type-provider-typebox";
import cors from "@fastify/cors";
import { db } from "./db";
import { mountEndpoints } from "./endpoints";

db.connect((error) => {
  if (error) {
    console.log(`Failed to connect to PostgreSQL`);
    throw error;
  } else {
    console.log(`Connected to PostgreSQL`);
  }
});

const server = fastify().withTypeProvider<TypeBoxTypeProvider>();

server.register(cors);

server.register(mountEndpoints);

server.listen({ port: Number(process.env.PORT || 3000), host: "::" });
