import { preHandlerAsyncHookHandler } from "fastify";
import { verify } from "jsonwebtoken";

export const auth: preHandlerAsyncHookHandler = async (req, res) => {
  const token = req.headers.authorization?.split(" ")[1];

  try {
    verify(token, process.env.JWT_SECRET || "default");
  } catch (e) {
    res.status(401).send({ message: "Unauthorized" });
  }
};
