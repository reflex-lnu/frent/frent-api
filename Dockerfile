FROM node:16.17.0 AS builder

WORKDIR frent-api/

COPY yarn.lock package.json ./

RUN yarn

RUN yarn add -E pg-native

COPY src src

RUN yarn build



FROM node:16.17.0-alpine

WORKDIR frent-api

COPY --from=builder frent-api/dist.js .

CMD ["node", "dist.js"]
