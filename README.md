# Frent API
База даних і серверний застосунок сервісу з оренди друзів **Frent**

[Картка з вимогами](https://gitlab.com/reflex-lnu/frent/frent-docs/-/issues/1)

# Database Design
Дошка з дизайном бази на dbdiabram.io - <https://dbdiagram.io/d/6356d125fa2755667d6126b8>

![Databas Design Image](db/design/frent.png)

## Database Deployment

### Creation
Для отримання sql-скрипта створення бази за поточним дизайном з *dbdiagram* - на дошці в панелі зверху вибираєте `Export` -> `Export to PostgreSQL`. Скачаний файл можете запустити в *pgAdmin* в базі (бажано пустій).

### Filling
В папці [db/scripts/mockaroo/inserts](db/scripts/mockaroo/inserts) знаходяться останні згенеровані інсерти для кожної таблиці окремо. Їх вміст є також з'єднаний в один файл [db/scripts/mockaroo/insert.sql](db/scripts/mockaroo/insert.sql). Виконання цього скрипта в PostgreSQL заповнить базу даними, які можна використовувати в процесі розробки і тестування.

Для генерації даних використовується сервіс [mockaroo](https://www.mockaroo.com). В папці [db/scripts/mockaroo](db/scripts/mockaroo) є python-скрипт `mockaroo-fetch.py`, який генерує `sql insert` скрипти на основі даних з відповідних схем на сервісі.

> ⚠️**Warning**  
> Запускати цей скрипт потрібно будучи в папці `db/scripts/mockaroo`, так як він оперує з файлами за шляхами відносно поточної директорії.
