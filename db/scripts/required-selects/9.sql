-- Вивести найманих друзiв у порядку спадання кiлькость скарг
-- вiд груп з принаймнi N клiєнтiв
-- за вказаний перiод (з дати F по дату T);
-- $1: F (events.party.starts_at)
-- $2: T (events.party.starts_at)
-- $3: N

-- friendId INT, friendFullName TEXT, complaints BIGINT
SELECT
    sub.friend_id as friendId,
    u.full_name as friendFullName,
    sub.n as complaints
FROM (
    SELECT c.party_friend_id, COUNT(*) as n
    FROM feedbacks.complaint as c
    INNER JOIN feedbacks.complaint_sign as cs
    ON cs.complaint_party_friend_id = c.party_friend_id
    WHERE c.created_at BETWEEN $1 AND $2
    HAVING COUNT(*) >= $3
) as sub
INNER JOIN users.friend as f
ON f.id = sub.friend_id
INNER JOIN users.user as u
ON u.id = f.user_id
ORDER BY complaints DESC;
