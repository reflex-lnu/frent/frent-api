--для найманого друга Х знайти усiх клiєнтiв, 
--якi наймали його принаймнi N разiв 
--за вказаний перiод (з дати F по дату T);
-- $1: X (users.friend.id)
-- $2: F (events.party.starts_at)
-- $3: T (events.party.starts_at)
-- $4: N

-- clientId INT, clientFullName TEXT, partyStartsAt TIMESTAMP, timesRented BIGINT
SELECT
    p.creator_client_id as clientId,
    u.full_name as clientFullName,
    p.starts_at as partyStartsAt,
    COUNT(*) as timesRented
FROM events.party AS p
INNER JOIN events.party_friend AS pf
ON pf.party_id = p.id
WHERE pf.friend_id = $1
AND p.starts_at BETWEEN $2 AND $3
GROUP BY p.creator_client_id, u.full_name, p.starts_at
HAVING COUNT(*) >= $4;