-- Знайти сумарну кiлькiсть побачень по мiсяцях

-- month INT, parties BIGINT
SELECT
    EXTRACT(MONTH FROM p.starts_at) as month,
    COUNT(*) as parties
FROM events.party as p
GROUP BY EXTRACT(MONTH FROM p.starts_at);
