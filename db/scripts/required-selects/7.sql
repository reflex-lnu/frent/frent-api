-- для найманого друга Х та кожного свята, на якому вiн побував, 
-- знайти скiльки разiв за вказаний перiод (з дати F по дату T) 
-- вiн був найнятий на свято у групi з принаймнi N друзiв
-- $1: X (users.friend.id)
-- $2: F (events.party.starts_at)
-- $3: T (events.party.starts_at)
-- $4: N

-- eventId INT, eventTitle TEXT, timesRented BIGINT
SELECT
    sub.event_id as eventId,
    e.title as eventTitle,
    COUNT(sub.party_id) AS timesRented
FROM (SELECT p.event_id AS event_id, p.id AS party_id
      FROM events.party_friend AS pf
      INNER JOIN events.party AS p ON pf.party_id = p.id
      LEFT JOIN events.event AS e ON e.id = p.event_id
      WHERE pf.friend_id = $1
        AND p.starts_at BETWEEN $2 AND $3
      GROUP BY p.event_id, p.id
      HAVING COUNT(*) > $4) AS sub
INNER JOIN events.event as e
ON e.id = sub.event_id
GROUP BY sub.event_id, e.title;
