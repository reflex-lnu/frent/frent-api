-- Знайти усi днi коли вихiдними були
-- вiд А до В найманих друзiв, включно
-- $1: A
-- $2: B

-- date DATE, friendsOnVacation BIGINT
SELECT
    v.date,
    COUNT(v.friend_id) as friendsOnVacation
FROM users.vacation as v
GROUP BY v.date
HAVING COUNT(v.friend_id) BETWEEN $1 AND $2;
