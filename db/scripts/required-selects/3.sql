--для найманого друга Х знайти усi свята, 
--на якi його наймали принаймнi N разiв 
--за вказаний перiод (з дати F по дату T);
-- $1: X (users.friend.id)
-- $2: F (events.party.starts_at)
-- $3: T (events.party.starts_at)
-- $4: N

-- eventId INT, eventTitle TEXT, timesRented BIGINT
SELECT
    p.event_id AS eventId,
    e.title as eventTitle,
    COUNT(*) as timesRented
FROM events.party AS p
INNER JOIN events.party_friend AS pf
ON p.id = pf.party_id
INNER JOIN events.event AS e
ON e.id = p.event_id
WHERE pf.id = $1
AND p.starts_at BETWEEN $2 AND $3
GROUP BY p.event_id, e.title
HAVING COUNT(*) >= $4;
