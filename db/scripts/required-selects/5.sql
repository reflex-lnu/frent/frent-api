--знайти усiх найманих друзiв, 
--яких наймали хоча б N разiв 
--за вказаний перiод (з дати F по дату T);
-- $1: F (events.party.starts_at)
-- $2: T (events.party.starts_at)
-- $3: N

-- friendId INT, friendFullName TEXT, partyStartsAt TIMESTAMP, timesRented BIGINT
SELECT
    pf.friend_id as friendId,
    f.full_name as friendFullName,
    p.starts_at as partyStartsAt,
    COUNT(*) as timesRented
FROM events.party_friend AS pf
INNER JOIN events.party AS p
ON pf.party_id = p.id
WHERE p.starts_at BETWEEN $1 AND $2
GROUP BY pf.friend_id
HAVING COUNT(*) >= $3;
