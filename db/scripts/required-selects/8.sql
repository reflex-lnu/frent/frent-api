-- вивести подарунки у порядку спадання середньої кiлькостi вихiдних,
-- що брали найманi друзi, якi отримували подарунок вiд клiєнта C
-- протягом вказаного перiоду (з дати F по дату T)
-- $1: C (users.client.id)
-- $2: F (events.party.starts_at)
-- $3: T (events.party.starts_at)

-- giftId INT, giftDescription TEXT, vacations NUMERIC
SELECT
    sub.gift_id as giftId,
    g.description,
    AVG(sub.vacations_count) as vacations
FROM (SELECT gv.gift_id, COUNT(v.*) AS vacations_count
      FROM gifts.giving AS gv
      INNER JOIN gifts.gift AS g ON gv.gift_id = g.id
      LEFT JOIN users.vacation AS v ON recipient_friend_id = v.friend_id
      WHERE g.creator_client_id = $1
        AND gv.given_at BETWEEN $2 AND $3
      GROUP BY gv.gift_id, gv.recipient_friend_id) AS sub
INNER JOIN gifts.gift as g
ON g.id = sub.gift_id
GROUP BY sub.gift_id
ORDER BY average_vacations_count DESC;
