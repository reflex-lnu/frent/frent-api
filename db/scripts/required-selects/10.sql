-- знайти усi спiльнi подiї
-- для клiєнта C та найманого друга Х
-- за вказаний перiод (з дати F по дату T);
-- $1: C (users.client.id)
-- $2: X (users.friend.id)
-- $3: F (events.party.starts_at)
-- $4: T (events.party.starts_at)

-- eventId INT, eventTitle TEXT, partyStartsAt TIMESTAMP
SELECT
    p.event_id as eventId,
    e.title as eventTitle,
    p.startsAt as partyStartsAt
FROM events.party as p
INNER JOIN events.event as e
ON e.id = p.event_id
WHERE p.creator_client_id = $3
  AND p.starts_at BETWEEN $3 AND $4
  AND p.id IN (SELECT pf.party_id FROM events.party_friend as pf WHERE pf.friend_id = $2);
