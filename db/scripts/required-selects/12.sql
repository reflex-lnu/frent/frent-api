-- по мiсяцях знайти середню кiлькiсть клiєнтiв у групi,
-- що реєстрували скаргу на найманого друга Х;
-- $1: X (users.friend.id)

-- month INT, complaints BIGINT, averageComplaintSigns NUMERIC
SELECT
    EXTRACT(MONTH FROM sub.created_at) as month,
    COUNT(*) as complaints,
    AVG(sub.n_signs) as averageComplaintSigns
FROM (
    SELECT c.created_at,
           COUNT(*) as n_signs
    FROM feedbacks.complaint as c
    INNER JOIN feedbacks.complaint_signer as cs
    ON cs.complaint_party_friend_id = c.party_friend_id
    INNER JOIN events.party_friend as pf
    ON pf.id = c.party_friend_id
    WHERE pf.friend_id = $1
     ) as sub
GROUP BY EXTRACT(MONTH FROM sub.created_at);
