-- знайти усiх клiєнтiв,
-- якi наймали щонайменше N рiзних друзiв
-- за вказаний перiод (з дати F по дату T);
-- $1: F (events.party.starts_at)
-- $2: T (events.party.starts_at)
-- $3: N

-- clientId INT, clientFullName TEXT, partyStartsAt TIMESTAMP, timesRented BIGINT
SELECT
    p.creator_client_id as clientId,
    u.full_name as clientFullName,
    p.starts_at as partyStartsAt,
    COUNT(pf.friend_id)
FROM events.party AS p
INNER JOIN users.user as u
ON u.id = p.creator_client_id
INNER JOIN events.party_friend AS pf
ON p.id = pf.party_id
WHERE p.starts_at BETWEEN $1 AND $2
GROUP BY p.creator_client_id, u.full_name, p.starts_at
HAVING COUNT(DISTINCT pf.friend_id) >= $3;
