-- для клiєнта С знайти усiх друзiв, 
-- яких вiн наймав принаймнi  N  разiв 
-- за вказаний перiод (з дати F по дату T);
-- $1: C (users.client.id)
-- $2: F (events.party.starts_at)
-- $3: T (events.party.starts_at)
-- $4: N

-- friendId INT, friendFullName TEXT, partyStartsAt TIMESTAMP, timesRented BIGINT
SELECT
    f.id as friendId,
    f.full_name as friendFullName,
    p.starts_at as partyStartsAt,
    COUNT(*) as timesRented
FROM users.friend AS f
INNER JOIN users.user as u
ON u.id = f.user_id
INNER JOIN events.party_friend AS pf
ON f.id = pf.friend_id
INNER JOIN events.party AS p
ON p.id = pf.party_id  
WHERE p.creator_client_id = $1
  AND p.starts_at BETWEEN $2 AND $3
GROUP BY f.id
HAVING COUNT(*) >= $4;
