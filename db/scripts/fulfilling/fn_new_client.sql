CREATE OR REPLACE FUNCTION fn_new_client(
    user_id INT
)
RETURNS TABLE (id int) AS
$BODY$
    INSERT INTO users.client
        (user_id)
    VALUES
        (user_id)
    RETURNING id;
$BODY$
LANGUAGE SQL;
