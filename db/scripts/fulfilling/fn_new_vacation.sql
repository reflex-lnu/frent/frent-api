CREATE OR REPLACE FUNCTION fn_new_vacation(
    friend_id INT,
    date DATE
)
RETURNS TABLE (
    friend_id INT, 
    date DATE
) AS
$BODY$
    INSERT INTO users.vacation
    (
        friend_id,
        date
    )
    VALUES
    (
        friend_id,
        date
    )
    RETURNING friend_id, date;
$BODY$
LANGUAGE SQL;
