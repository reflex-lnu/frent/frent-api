CREATE OR REPLACE FUNCTION fn_new_user(
    first_name TEXT,
    last_name TEXT,
    birth_date DATE,
    sex users.SEX,
    email TEXT,
    phone_number INT,
    address TEXT
)
RETURNS TABLE (id INT) AS
$BODY$
    INSERT INTO users.user
    (
        first_name,
        last_name,
        birth_date,
        sex,
        email,
        phone_number,
        address
    )
    VALUES
    (
        first_name,
        last_name,
        birth_date,
        sex,
        email,
        phone_number,
        address
    )
    RETURNING id;
$BODY$
LANGUAGE SQL;
