CREATE OR REPLACE FUNCTION fn_new_party(
    event_id INT,
    title TEXT,
    description TEXT,
    creator_client_id INT,
    start_ts TIMESTAMP,
    duration INTERVAL,
    online BOOLEAN,
    details TEXT
)
RETURNS TABLE (id INT) AS
$BODY$
    INSERT INTO events.party
    (
        event_id,
        title,
        description,
        creator_client_id,
        start_ts,
        duration,
        online,
        details
    )
    VALUES
    (
        event_id,
        title,
        description,
        creator_client_id,
        start_ts,
        duration,
        online,
        details
    )
    RETURNING id;
$BODY$
LANGUAGE SQL;
