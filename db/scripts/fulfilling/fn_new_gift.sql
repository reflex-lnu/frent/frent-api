CREATE OR REPLACE FUNCTION fn_new_gift(
    creator_client_id INT,
    description TEXT
)
RETURNS TABLE (id INT) AS
$BODY$
    INSERT INTO gifts.gift
    (
        creator_client_id,
        description
    )
    VALUES
    (
        creator_client_id,
        description
    )
    RETURNING id;
$BODY$
LANGUAGE SQL;
