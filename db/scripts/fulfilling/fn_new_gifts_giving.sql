CREATE OR REPLACE FUNCTION fn_new_gifts_giving(
    gift_id INT,
    recipient_friend_id INT,
    returned_ts TIMESTAMP
)
RETURNS TABLE (
    gift_id INT, 
    recipient_friend_id INT, 
    returned_ts TIMESTAMP
) AS
$BODY$
    INSERT INTO gifts.giving
    (
        gift_id,
        recipient_friend_id,
        returned_ts
    )
    VALUES
    (
        gift_id,
        recipient_friend_id,
        returned_ts
    )
    RETURNING gift_id, recipient_friend_id, returned_ts
$BODY$
LANGUAGE SQL;
