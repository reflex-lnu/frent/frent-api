WITH
new_client_user AS (
    SELECT id
    FROM fn_new_user(
        'Oleksiy',
        'Client',
        '2000-05-05',
        'male',
        'test@test.com',
        '0123456',
        'Lviv, Stryiska 123a'
    )
),
new_friend_user AS (
    SELECT id
    FROM fn_new_user(
        'Iryna',
        'Friend',
        '2000-05-05',
        'female',
        'test2@test.com',
        '0123452',
        'Lviv, Stryiska 983a'
    )
),
new_client AS (
    SELECT id
    FROM fn_new_client(new_client_user.id)
),
new_friend AS (
    SELECT id
    FROM fn_new_friend(new_friend_user.id)
),
--new_vacation AS(
--	SELECT friend_id, date FROM public.create_vacation((SELECT id FROM new_friend), '2021-09-27')
--),
new_event AS (
    SELECT id
    FROM fn_new_event(
        'Birthday',
        'Birthday party',
        '2022-11-03 00:00:00',
        make_interval(hours => 10),
        false
    )
),
new_party AS (
    SELECT id
    FROM fn_new_party(
        new_event.id,
        'Birthday',
        'Birthday party',
        new_client.id,
        '2022-11-03 00:00:00',
        make_interval(hours => 10),
        false,
        'At my house'
    )
),
new_party_friend AS (
    SELECT
        party_id,
        friend_id
    FROM fn_new_party_friend(
        new_party.id,
        new_friend.id
    )
),
new_complaint AS (
    SELECT id
    FROM fn_new_complaint(
        new_client.id,
        new_friend.id,
        'Good guest',
        new_party.id
    )
),

new_complaint_signer AS(
    SELECT
        complaint_id,
        client_id
    FROM fn_new_complaint_signer(
        new_complaint.id,
        new_client.id
    )
),
new_gift AS(
    SELECT id
    FROM fn_new_gift(
        new_client.id,
        'good gift'
    )
),
new_gifts_giving AS(
    SELECT
        gift_id,
        recipient_friend_id,
        returned_ts
    FROM fn_new_gifts_giving(
        new_gift.id,
        new_friend.id,
        '2022-11-03 00:00:00'
    )
)

SELECT
    e.id as event_id,
    p.id AS party_id,
    u.id AS party_creator_id,
    u.first_name AS party_creator_first_name,
    u.last_name AS party_creator_last_name
FROM events.event AS e
INNER JOIN events.party AS p ON p.event_id = e.id
INNER JOIN users.client AS c ON c.id = p.creator_client_id
INNER JOIN users.user AS u ON u.id = c.user_id;
