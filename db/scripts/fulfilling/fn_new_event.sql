CREATE OR REPLACE FUNCTION fn_new_event(
    title TEXT,
    description TEXT,
    start_ts TIMESTAMP,
    duration INTERVAL,
    recurring BOOLEAN
)
RETURNS TABLE (id INT) AS
$BODY$
    INSERT INTO events.event
    (
        title,
        description,
        start_ts,
        duration,
        recurring
    )
    VALUES
    (
        title,
        description,
        start_ts,
        duration,
        recurring
    )
    RETURNING id;
$BODY$
LANGUAGE SQL;
