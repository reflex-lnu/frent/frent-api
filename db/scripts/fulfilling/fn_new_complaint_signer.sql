CREATE OR REPLACE FUNCTION fn_new_complaint_signer(
    complaint_id INT,
    client_id INT
)
RETURNS TABLE (
    complaint_id INT, 
    client_id INT
) AS
$BODY$
    INSERT INTO feedbacks.complaint_signer
    (
        complaint_id,
        client_id
    )
    VALUES
    (
        complaint_id,
        client_id
    )
    RETURNING complaint_id, client_id;
$BODY$
LANGUAGE SQL;
