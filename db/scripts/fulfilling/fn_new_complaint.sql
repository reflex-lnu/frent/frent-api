CREATE OR REPLACE FUNCTION fn_new_complaint(
    creator_client_id INT,
    friend_id INT,
    content TEXT,
    party_id INT
)
RETURNS TABLE (id INT) AS
$BODY$
    INSERT INTO feedbacks.complaint
    (
        creator_client_id,
        friend_id,
        content,
        party_id
    )
    VALUES
    (
        creator_client_id,
        friend_id,
        content,
        party_id
    )
    RETURNING id;
$BODY$
LANGUAGE SQL;
