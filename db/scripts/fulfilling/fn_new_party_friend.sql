CREATE OR REPLACE FUNCTION fn_new_party_friend(
    party_id INT, 
    friend_id INT
)
RETURNS TABLE (
    party_id INT, 
    friend_id INT
) AS
$BODY$
    INSERT INTO events.party_friend
    (
        party_id, 
        friend_id
    )
    VALUES
    (
        party_id, 
        friend_id
    )
    RETURNING party_id, friend_id;
$BODY$
LANGUAGE SQL;
