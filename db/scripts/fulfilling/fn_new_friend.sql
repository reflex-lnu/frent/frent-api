CREATE OR REPLACE FUNCTION fn_new_friend(
    user_id INT
)
RETURNS TABLE (id int) AS
$BODY$
    INSERT INTO users.friend
        (user_id)
    VALUES
        (user_id)
    RETURNING id;
$BODY$
LANGUAGE SQL;
