import requests
import json


MAIN_INSERT_FILE = 'inserts.sql'
SCHEMAS_DIR = 'schemas'
INSERTS_DIR = 'inserts'
URL_TEMPLATE = 'https://api.mockaroo.com/api/{schema_id}?count={num_rows}&key=1653d850'

RED = "\033[31m"
GREEN = "\033[32m"
YELLOW = "\033[33m"
BLUE = "\033[34m"
MAGENTA = "\033[35m"
RESET = "\033[0m"


public_ids = {
    'users.user': 'deee3910',
    'users.client': '70db4640',
    'users.friend': '953de480',
    'users.vacation': 'a661b980',
    'events.event': '6ac819a0',
    'events.party': '52048300',
    'events.party_friend': '7e426ea0',
    'feedbacks.complaint': '1b9651d0',
    'feedbacks.complaint_sign': '5ce7ab00',
    'gifts.gift': 'a55650a0',
    'gifts.giving': 'b2f29050'
}


def strip_margin(s: str):
    return '\n'.join(l.strip() for l in s.strip().split())


def get_num_rows(schema_name: str):
    with open(f'{SCHEMAS_DIR}/{schema_name}.schema.json') as file:
        return json.load(file)['num_rows']


def get_url(schema_name: str):
    return URL_TEMPLATE.format(schema_id=public_ids[schema_name], num_rows=get_num_rows(schema_name))


def fetch(schema_name: str):
    url = get_url(schema_name)
    r = requests.get(url)
    if not r.ok:
        raise ValueError(strip_margin(
            f"""{RED}Failed to fetch {schema_name} schema.{RESET}
            Url: {url}.
            Response code: {r.status_code}.
            Response: {r.text}"""
        ))
    return r.text


def merge_inserts(s: str):
    inserts = [i.rstrip(';').split(' values ') for i in s.strip().split('\n')]
    sep = ',\n\t'
    return f'{inserts[0][0]} values\n\t{sep.join(i[1] for i in inserts)};\n'


def blue(s: str):
    return f"{BLUE}{s}{RESET}"


def cprint(s: str, color: str):
    print(f"{color}{s}{RESET}")


def main():
    with open(MAIN_INSERT_FILE, 'w') as inserts:
        for schema_name in public_ids:
            filename = f'{INSERTS_DIR}/{schema_name}.sql'
            with open(filename, 'w') as file:
                cprint(f"Fetching {schema_name} schema...", BLUE)
                data = merge_inserts(fetch(schema_name))
                cprint(f"Writing to {filename}...", YELLOW)
                file.write(data)
                cprint(f"Writing to {MAIN_INSERT_FILE}...", MAGENTA)
                inserts.write(f'-- {schema_name}\n{data}\n')
                cprint("SUCCESS!\n", GREEN)


if __name__ == '__main__':
    main()
